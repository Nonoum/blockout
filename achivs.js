// achievements controller

achivs = (function() {
	var ac = [];
	ac.init = function(params) {
		if(undefined != this.h_impl) this.h_impl.init(params);
	};
	ac.onScoreAdded = function(total_score) {
		if(undefined != this.h_impl) this.h_impl.onScoreAdded(total_score);
	};
	ac.onLayersCleaned = function(n_cleaned, layers_info) {
		if(undefined != this.h_impl) this.h_impl.onLayersCleaned(n_cleaned, layers_info);
	};
	ac.onTimeAdded = function(total_time_seconds) {
		if(undefined != this.h_impl) this.h_impl.onTimeAdded(total_time_seconds);
	};
	return ac;
})();