/*
	Function for creating animator object.
	(data): {pos: figure pos,
			repr: figure representation,
			size: graphical size of one block of figure,
			drawable: drawable object that represents figure,
			tonnel: game tonnel};
	(type): "move" or "rotate";
	params for (type)=="move": dir: string [xy][+-];
	params for (type)=="rotate": dir: string [xyz][+-];

	returns animator object (and updates data's 'pos' and 'repr' fields), if it's possible, otherwise returns null.
*/
function makeAnimator(data, type, params) {
	var anim = [];
	anim.h_time_total = consts.getAnimationTime(props.getLevel());
	anim.h_time_left = anim.h_time_total;
	anim.h_size = data.size;
	anim.h_missed_delta = 0;

	anim.process = function(drawable, time_passed, had_conflicted) {
		var result = this.h_time_left <= time_passed;
		if(time_passed > this.h_time_left) {
			time_passed = this.h_time_left;
			this.h_time_left = 0;
		} else {
			this.h_time_left -= time_passed;
		}
		this.h_missed_delta += time_passed / this.h_time_total;
		if(! had_conflicted) {
			this.h_process(drawable, this.h_missed_delta);
			this.h_missed_delta = 0;
			return result;
		}
		return false;
	};

	if("move" == type) {
		return h_makeMovementAnimator(anim, data, params);
	} else if("rotate" == type) {
		return h_makeRotationAnimator(anim, data, params);
	} else {
		nn_assert(false, 'incorrect type of animator');
	}
	return null;
};

function h_makeMovementAnimator(anim, data, params) {
	anim.h_dx = 0;
	anim.h_dy = 0;
	anim['h_d' + params.dir[0]] = ('+' == params.dir[1]) ? 1 : -1;

	pos = {x: data.pos.x + anim.h_dx, y: data.pos.y + anim.h_dy, z: data.pos.z };
	affect = checkFigureAffectsTonnel(data.tonnel, pos, data.repr);
	if('cross' == affect) {
		return null; // not possible
	}
	data.pos.x += anim.h_dx;
	data.pos.y += anim.h_dy;

	anim.isConflictable = function() {
		return false;
	};
	anim.h_process = function(drawable, delta) {
		drawable.position.x += this.h_dx * delta * this.h_size;
		drawable.position.y += this.h_dy * delta * this.h_size;
	};
	return anim;
};

/*
	A function from internet..
	workaround for incorrect rotation in Three.js.
*/
function rotateAroundWorldAxis(object, axis, radians) {
    rotWorldMatrix = new THREE.Matrix4();
    rotWorldMatrix.makeRotationAxis(axis.normalize(), radians);
    rotWorldMatrix.multiply(object.matrix);

    object.matrix = rotWorldMatrix;
    object.rotation.setFromRotationMatrix(object.matrix);
};

function h_makeRotationAnimator(anim, data, params) {
	anim.h_drx = 0;
	anim.h_dry = 0;
	anim.h_drz = 0;
	anim['h_dr' + params.dir[0]] = 1;
	anim.h_sign = ('+' == params.dir[1]) ? 1 : -1;

	var rot_repr = h_makeRotatedRepresentation(data.repr, params.dir);
	var fixed_pos = h_getFixedPosition(data.tonnel, data.pos, rot_repr);
	affect = checkFigureAffectsTonnel(data.tonnel, fixed_pos, rot_repr);
	if('cross' == affect) {
		return null; // not possible
	}
	// calculating position differences
	anim.h_dx = fixed_pos.x - data.pos.x;
	anim.h_dy = fixed_pos.y - data.pos.y;
	anim.h_dz = fixed_pos.z - data.pos.z;
	// updating source's position
	data.pos.x += anim.h_dx;
	data.pos.y += anim.h_dy;
	data.pos.z += anim.h_dz;
	// updating source's representation
	copyArray3d(rot_repr, data.repr, rot_repr.length, rot_repr.length, rot_repr.length);

	anim.isConflictable = function() {
		// This means that two conflictable animators can't be processed correctly simultaneously
		// because currently Three.js only allows to rotate Mesh with matrix operations,
		// and doesn't allow to set several 'rotation' for more than one axis simultaneously.
		// Rotating Mesh is incorrect in case of simultaneous rotates, so next conflictable animator
		// have to wait until previous one get processed.
		return true;
	};
	anim.h_process = function(drawable, delta) {
		var ax = new THREE.Vector3(this.h_drx, this.h_dry, this.h_drz);
		rotateAroundWorldAxis(drawable, ax, this.h_sign * (Math.PI / 2) * delta);
		drawable.position.x += this.h_dx * delta * this.h_size;
		drawable.position.y += this.h_dy * delta * this.h_size;
		drawable.position.z += this.h_dz * delta * this.h_size;
	};
	return anim;
};

function h_makeRotatedRepresentation(repr, dir) {
	var r = makeArray3d(repr.length, repr.length, repr.length, false);
	function rotatedIndexes(size, a, b) {
		return {b: size-1-a, a: b};
	};
	var mappers = [];
	mappers['x+'] = function(size, x, y, z) { return {x: x, y: z, z: size-1-y}; };
	mappers['x-'] = function(size, x, y, z) { return {x: x, y: size-1-z, z: y}; };
	mappers['y+'] = function(size, x, y, z) { return {x: size-1-z, y: y, z: x}; };
	mappers['y-'] = function(size, x, y, z) { return {x: z, y: y, z: size-1-x}; };
	mappers['z+'] = function(size, x, y, z) { return {x: y, y: size-1-x, z: z}; };
	mappers['z-'] = function(size, x, y, z) { return {x: size-1-y, y: x, z: z}; };
	var mpr = mappers[dir];
	nn_assert(mpr != undefined, 'Wrong rotation direction');

	var sz = repr.length; // same size for every coordinate
	for(var x = 0; x < sz; x++) {
		for(var y = 0; y < sz; y++) {
			for(var z = 0; z < sz; z++) {
				mapped = mpr(sz, x, y, z);
				r[x][y][z] = repr[mapped.x][mapped.y][mapped.z];
			}
		}
	}
	return r;
};

function h_getFixedPosition(tonnel, pos, repr) {
	var result = {x: pos.x, y: pos.y, z: pos.z};
	var tx = tonnel.length;
	var ty = tonnel[0].length;
	var repr_bounds = (function() {
		var size = repr.length; // same size for every coordinate
		function yzClear(x) {
			for(var y = 0; y < size; y++) {
				for(var z = 0; z < size; z++) {
					if(repr[x][y][z]) {
						return false;
					}
				}
			}
			return true;
		};
		function xzClear(y) {
			for(var x = 0; x < size; x++) {
				for(var z = 0; z < size; z++) {
					if(repr[x][y][z]) {
						return false;
					}
				}
			}
			return true;
		};
		bs = [];
		for(bs.xl = 0; bs.xl < size && yzClear(bs.xl); bs.xl++) {
		}; // x left bound done
		for(bs.xr = size-1; bs.xr >= 0 && yzClear(bs.xr); bs.xr--) {
		}; // x right
		for(bs.yd = 0; bs.yd < size && xzClear(bs.yd); bs.yd++) {
		}; // y down
		for(bs.yu = size-1; bs.yu >= 0 && xzClear(bs.yu); bs.yu--) {
		}; // y up
		return bs;
	})();
	//alert('' + repr_bounds.xl + ' ' + repr_bounds.xr + '.');
	while(result.x + repr_bounds.xr >= tx) { result.x--; };
	while(result.x + repr_bounds.xl < 0) { result.x++; };
	while(result.y + repr_bounds.yu >= ty) { result.y--; };
	while(result.y + repr_bounds.yd < 0) { result.y++; };
	return result;
};