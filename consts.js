// project's constants.

consts = (function () {
	var c = [];
	// graphical constants
	c.game_fillcolor = 0x303030;
	c.bars_bg_style = "rgb(48, 48, 48)";
	c.bars_strokestyle = "rgb(0, 255, 255)";
	c.bars_static_text_style = "rgb(255, 255, 192)";
	c.bars_dynamic_text_style = "rgb(255, 255, 0)";
	c.bars_message_text_style = "rgb(192, 32, 32)";

	c.getAmbientLight = function() {
		return new THREE.AmbientLight( 0xe0e0e0 );
	};
	c.grid_material = new THREE.LineBasicMaterial( { color: 0x00c000, opacity: 0.5 } );
	c.figure_material = new THREE.LineBasicMaterial( { color: 0xFFFFFF, opacity: 1.0 } );
	c.fill_elem_line_material = new THREE.LineBasicMaterial( { color: 0xFFFFFF, opacity: 0.75, linewidth: 1.0 } );
	{
		var materials = [];
		var colors = [0x0000A0, 0x00A000, 0x00A0A0, 0xA00000, 0xA000A0, 0xA0A000,
						0x0000C0, 0x00C000, 0x00C0C0, 0xA00000, 0xC000C0, 0xC0C000];
		for(var i = 0; i < colors.length; i++) {
			materials[i] = new THREE.MeshLambertMaterial( { color: colors[i], overdraw: 0.5 } );
		}
		c.h_fill_elem_plane_colors = colors;
		c.h_fill_elem_plane_materials = materials;
		c.getFillElemPlaneMaterial = function(depth) {
			depth = depth % this.h_fill_elem_plane_materials.length;
			return this.h_fill_elem_plane_materials[depth];
		};
		c.getFillElemColor = function(depth) {
			depth = depth % this.h_fill_elem_plane_colors.length;
			return this.h_fill_elem_plane_colors[depth];
		};
	}
	// physical constants
	c.max_level = 9;
	c.h_figure_freefall_speed = 42; // speed for figure on free fall
	c.h_figure_fall_speed = 0.2; // basic speed of falling
	c.h_animation_time_seconds = 0.125; // time per animation in seconds (not completed value - divides on complication level)
	c.getAnimationTime = function(level) {
		nn_assert(level >= 0 && level <= this.max_level, "Incorrect level: " + level);
		return this.h_animation_time_seconds / Math.pow(level+1, 0.33);
	};
	c.getFallingSpeed = function(level, is_free_fall) {
		is_free_fall = is_free_fall || false;
		nn_assert(level >= 0 && level <= this.max_level, "Incorrect level: " + level);
		return (true == is_free_fall ? this.h_figure_freefall_speed : this.h_figure_fall_speed * Math.pow(level+1, 1.5));
	};
	c.getLevelBlocksBound = function(level) {
		nn_assert(level >= 0 && level <= this.max_level, "Incorrect level: " + level);
		return 1000 * (level) - 75 * Math.floor((level + level * level) / 2);
	};
	// common constants
	c.metrices = [[3,3,12], [3,3,18], [3,3,22], [4,4,16], [5,5,18]];
	return c;
})();