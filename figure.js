/*
	Function for creating complete figure object.
	(raw_geometry) - raw geometry data, taken from 'geometries'.
	(graphic_size) - graphic size of single block in figure.

	returns formed figure object.
*/
function makeFigure(raw_geometry, graphic_size) {
	var fg = [];
	fg.h_animators = [];
	fg.h_pos = {x: 0, y: 0, z: 0};
	fg.h_graphic_size = graphic_size;
	fg.h_repr = generateFigureRepr(raw_geometry[0], raw_geometry[1]);
	fg.h_drawable = generateFigureDrawable(fg.h_repr, graphic_size);
	// methods -----------------------
	fg.sceneAdd = h_addFigureToScene;
	fg.sceneRemove = h_removeFigureFromScene;
	fg.place = h_placeFigure;
	// behaviour
	fg.action = h_actionFigure;
	fg.process = h_processFigure;
	// other initialization ----------------
	fg.h_speed = consts.getFallingSpeed(props.getLevel());
	fg.place(0, 0, 0);
	return fg;
};

// -------------------------------

function h_addFigureToScene(scene) {
	scene.add( this.h_drawable );
};

function h_removeFigureFromScene(scene) {
	scene.remove( this.h_drawable );
};

function h_placeFigure(x, y, z) {
	this.h_pos = {x: x, y: y, z: z};
	this.h_drawable.position.set(x, y, z);
	this.h_drawable.position.multiplyScalar(this.h_graphic_size);
	this.h_drawable.position.addScalar(0.5 * this.h_repr.length * this.h_graphic_size);
};

function h_actionFigure(type, params, tonnel, done_catcher) {
	if("freefall" == type) {
		this.h_speed = consts.getFallingSpeed(props.getLevel(), true);
	} else if("move" == type || "rotate" == type) {
		var data = [];
		data.pos = this.h_pos;
		data.repr = this.h_repr;
		data.size = this.h_graphic_size;
		data.drawable = this.h_drawable;
		data.tonnel = tonnel;
		animator = makeAnimator(data, type, params);
		if(null != animator) {
			affect = checkFigureAffectsTonnel(tonnel, this.h_pos, this.h_repr);
			if('fit' == affect) {
				var alist = h_appendFigureInTonnel(this, tonnel);
				done_catcher('fit', {list: alist});
				return;
			}
			nn_assert('cross' != affect, 'oops, incorrect work of animator');
			this.h_animators.push(animator);
		}
	}
};

function h_processFigure(tonnel, done_catcher, time_passed) {
	to_add = this.h_speed * time_passed;
	this.h_drawable.position.z += to_add * this.h_graphic_size;
	// processing fall
	for( ; to_add > 0; ) {
		affect = checkFigureAffectsTonnel(tonnel, this.h_pos, this.h_repr);
		if('cross' == affect || 'fit' == affect) {
			var alist = h_appendFigureInTonnel(this, tonnel);
			done_catcher(affect, {list: alist});
			return;
		}
		if(to_add < 1) {
			this.h_pos.z += to_add;
			to_add = 0;
		} else {
			this.h_pos.z += 1;
			to_add -= 1;
		}
	}
	anims_cnt = this.h_animators.length;
	had_conflicted = false;
	for(var i = 0; i < anims_cnt; i++) {
		anim = this.h_animators[i];
		if(anim.process(this.h_drawable, time_passed, had_conflicted)) {
			// animator completed, removing it
			this.h_animators.splice(i, 1);
			anims_cnt--;
			i--;
		}
		had_conflicted |= anim.isConflictable();
	}
};

function h_appendFigureInTonnel(figure, tonnel) {
	var cx = figure.h_repr.length;
	var cy = figure.h_repr[0].length;
	var cz = figure.h_repr[0][0].length;
	var alist = [];

	for(var ix = 0; ix < cx; ix++) {
		x = figure.h_pos.x + ix;
		for(var iy = 0; iy < cy; iy++) {
			y = figure.h_pos.y + iy;
			for(var iz = 0; iz < cz; iz++) {
				z = Math.floor(figure.h_pos.z) + iz;
				if(figure.h_repr[ix][iy][iz]) {
					if(! tonnel[x][y][z]) {
						alist.push( {x: x, y: y, z: z} );
					}
					tonnel[x][y][z] = true;
				}
			}
		}
	}
	return alist;
};

/*
	Common function for checks how does figure affect tonnel.
	(tonnel) - game tonnel to check;
	(pos) - x,y,z position of figure in tonnel;
	(fg_repr) - figure representation.

	returns one of the following strings: 'cross', 'fit', 'free' according to figure/tonnel relation.
*/
function checkFigureAffectsTonnel(tonnel, pos, fg_repr) {
	var cx = fg_repr.length;
	var cy = fg_repr[0].length;
	var cz = fg_repr[0][0].length;
	var tonnel_cz = tonnel[0][0].length;
	var fits = false;
	for(var ix = 0; ix < cx; ix++) {
		x = pos.x + ix;
		for(var iy = 0; iy < cy; iy++) {
			y = pos.y + iy;
			z = -1;
			z_column_ref = fg_repr[ix][iy];
			for(var iz = cz-1; iz >= 0; iz--) {
				if(z_column_ref[iz]) {
					z = iz;
					break;
				}
			}
			if(-1 == z) { // no voxels at this z-column
				continue;
			}
			z = Math.floor(pos.z) + z;
			if((undefined == tonnel[x]) ||
				(undefined == tonnel[x][y]) ||
				(undefined == tonnel[x][y][z]) ||
				(true == tonnel[x][y][z])) {
				return 'cross'; // figure crosses blocks in tonnel
			}
			if((z+1 == tonnel_cz) || (z+1 < tonnel_cz && tonnel[x][y][z+1])) {
				fits = true;
			}
		}
	}
	if(fits) {
		return 'fit';
	}
	return 'free';
};