// workaround for cross-browser animation.
// creates 'n_requestAnimFrame' function-property in 'window' for requesting animation.
// usage:
//		function redraw_function() { ... };
//		window.n_requestAnimFrame(redraw_function);

(function () {
	var fnames = ["requestAnimationFrame", "webkitRequestAnimationFrame",
		"mozRequestAnimationFrame", "oRequestAnimationFrame", "msRequestAnimationFrame"];
	var fun = undefined;
	function setUp(f) {
		window.n_requestAnimFrame = f;
	};
	for(var i = 0; i < fnames.length; i++) {
		var f = window[fnames[i]];
		if(f != undefined && f != null) {
			setUp(f);
			return;
		}
	}
	lambda_timer = function(cback, element) {
		window.setTimeout(cback, 1000 / 60); // 60 fps
	};
	setUp(lambda_timer);
})();