// The game properties

props = (function() {
	var p = [];
	p.h_level = 0;
	p.h_metric_idx = 0;
	p.h_score = 0;
	p.h_blocks = 0;
	p.h_set_id = 0; // id of current figure set
	p.h_uri_params = [];
	// accessors
	p.getLevel = function () {
		return this.h_level;
	};
	p.setLevel = function(lvl) {
		nn_assert(lvl >= 0 && lvl <= consts.max_level, "Incorrect level: " + lvl);
		this.h_level = lvl;
	};
	p.getMetric = function() {
		return consts.metrices[this.h_metric_idx];
	};
	p.getMetricIndex = function() {
		return this.h_metric_idx;
	};
	p.setMetricIndex = function(idx) {
		nn_assert(idx >= 0 && idx < consts.metrices.length, 'Incorrect metric index');
		this.h_metric_idx = idx;
	};
	p.getSetId = function () {
		return this.h_set_id;
	};
	p.setSetId = function(set_id) {
		this.h_set_id = set_id;
	};
	p.getScore = function() {
		return this.h_score;
	};
	p.setScore = function(score) {
		this.h_score = score;
	};
	p.getBlocksCount = function() {
		return this.h_blocks;
	};
	p.setBlocksCount = function(count) {
		this.h_blocks = count;
	};
	// uri
	p.getUriParams = function() {
		return this.h_uri_params;
	};
	p.resetUriParams = function() {
		this.h_uri_params = [];
	};
	p.appendUriParams = function(params_dict) {
		params_dict = params_dict || [];
		keys = Object.keys(params_dict);
		for(var i = 0; i < keys.length; i++) {
			this.h_uri_params[keys[i]] = params_dict[keys[i]];
		}
	};
	// serialization
	p.get = function() {
		return getPropertiesUri(this, this.h_uri_params, ['h_level', 'h_metric_idx', 'h_set_id'], {h_level: 'l', h_metric_idx: 'm', h_set_id: 's'});
	};
	p.set = function(uri) {
		this.h_uri_params = parseUriParams(uri);
		l = parseInt(this.h_uri_params['l'] || this.h_level);
		m = parseInt(this.h_uri_params['m'] || this.h_metric_idx);
		s = parseInt(this.h_uri_params['s'] || this.h_set_id);
		this.h_level = l;
		this.h_metric_idx = m;
		this.h_set_id = s;
	};
	return p;
})();