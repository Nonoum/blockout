// the game engine implementation.

game = (function() {
	var g = [];
	return g;
})();

/*
	Initializes 'game' object with appropriate parameters, game supposed to be started immediately.
	(width) - width of game tonnel in blocks;
	(height) - height of game tonnel in blocks;
	(depth) - depth of game tonnel in blocks;
	(renderer) - Three.js renderer object.
*/
function initGame(width, height, depth, renderer) {
	game.h_wi = width;
	game.h_he = height;
	game.h_de = depth;
	game.h_size_multiplier = 100.0 / width;

	game.h_camera = new THREE.PerspectiveCamera( 60, 1, 1, 10000 );
	game.h_camera.position.set(50, 50, -87);
	game.h_camera.lookAt(new THREE.Vector3( 50, 50, 1000 ));
	game.h_renderer = renderer;
	game.h_prev_time = getTimeMillis();
	game.h_paused = false;
	game.h_seconds_played = 0;
	game.h_start_level_blocks_bound = consts.getLevelBlocksBound(props.getLevel());
	game.h_should_exit = false;
	// objects etc
	h_initTonnel();
	h_initScene();
	h_initBlocksData();
	// functions
	game.keyDown = h_gameKeyDown;
	game.onTimer = h_gameTimerMethod;
	game.getLayersInfo = h_gameGetLayersInfo;
	game.shouldExit = function() {
		return this.h_should_exit;
	};
};

function h_gamePutFigure() {
	game.h_figure = makeFigure(geometries.getRandomFigure(props.getSetId()), game.h_size_multiplier);
	game.h_figure.place(0, 0, 0);
	game.h_figure.sceneAdd( game.h_scene );
};

function h_initScene() {
	game.h_scene = new THREE.Scene();
	game.h_scene.add(consts.getAmbientLight());

	h_gamePutFigure();
	game.h_scene.add( makeTonnelGrid(game.h_wi, game.h_he, game.h_de, game.h_size_multiplier, 0, 0, 0) );
};

function h_initTonnel() {
	game.h_tonnel = makeArray3d(game.h_wi, game.h_he, game.h_de, false);
};

function h_initBlocksData() {
	game.h_layers_blocks = []; // stores lists of drawable objects for each layer
	for(var i = 0; i < game.h_de; i++) {
		game.h_layers_blocks[i] = [];
	}
};

function h_gameKeyDown(event) {
	switch( event.keyCode ) {
		case 80: h_gamePause(); break; // P
		case 27: this.h_should_exit = true; break; // ESC - exit
	}
	if(undefined == this.h_figure || this.h_paused) {
		return;
	}
	switch( event.keyCode ) {
		case 37: this.h_figure.action("move", {dir: 'x+'}, this.h_tonnel, h_onFigureDone); break; // left
		case 39: this.h_figure.action("move", {dir: 'x-'}, this.h_tonnel, h_onFigureDone); break; // right
		case 38: this.h_figure.action("move", {dir: 'y+'}, this.h_tonnel, h_onFigureDone); break; // up
		case 40: this.h_figure.action("move", {dir: 'y-'}, this.h_tonnel, h_onFigureDone); break; // down

		case 32: this.h_figure.action("freefall", [], this.h_tonnel, h_onFigureDone); break; // space

		case 81: this.h_figure.action("rotate", {dir: 'x-'}, this.h_tonnel, h_onFigureDone); break; // Q
		case 65: this.h_figure.action("rotate", {dir: 'x+'}, this.h_tonnel, h_onFigureDone); break; // A
		case 87: this.h_figure.action("rotate", {dir: 'y+'}, this.h_tonnel, h_onFigureDone); break; // W
		case 83: this.h_figure.action("rotate", {dir: 'y-'}, this.h_tonnel, h_onFigureDone); break; // S
		case 69: this.h_figure.action("rotate", {dir: 'z-'}, this.h_tonnel, h_onFigureDone); break; // E
		case 68: this.h_figure.action("rotate", {dir: 'z+'}, this.h_tonnel, h_onFigureDone); break; // D
	}
};

function h_gameTimerMethod() {
	if(this.h_paused) {
		return;
	}
	if(undefined != this.h_figure) {
		var time = getTimeMillis();
		var time_passed = (time - this.h_prev_time) / 1000.0;
		this.h_prev_time = time;
		this.h_seconds_played += time_passed;
		this.h_figure.process(this.h_tonnel, h_onFigureDone, time_passed);

		var info = this.getLayersInfo();
		bars.updateLayers(info.max_present);
		bars.updateTimePlayed(this.h_seconds_played);
		achivs.onTimeAdded(this.h_seconds_played);

		this.h_renderer.render( this.h_scene, this.h_camera );
	}
};

function h_gameGetLayersInfo() {
	var cx = this.h_tonnel.length;
	var cy = this.h_tonnel[0].length;
	var cz = this.h_tonnel[0][0].length;
	function xyClear(tonnel, z) {
		for(var x = 0; x < cx; x++) {
			for(var y = 0; y < cy; y++) {
				if(tonnel[x][y][z]) {
					return false;
				}
			}
		}
		return true;
	};
	var flags = [];
	var max = -1;
	for(var z = cz-1; z >= 0; z--) {
		var present = ! xyClear(this.h_tonnel, z);
		flags.push( present );
		if(present) {
			max = flags.length - 1;
		}
	}
	return { flags: flags, max_present: max };
};

function h_onFigureDone(result, params) { // call when figure have fit into tonnel
	game.h_figure.sceneRemove(game.h_scene);
	scorer.onFigurePlaced(params.list.length);
	var level = props.getLevel();
	if(level < consts.max_level &&
		(props.getBlocksCount() + game.h_start_level_blocks_bound) >= consts.getLevelBlocksBound(level+1)) {

		props.setLevel(level+1);
	}

	h_gameAppendBlocks(params.list || []);
	if('fit' == result) {
		h_gameProcessTonnel();
		h_gamePutFigure();
	} else if('cross' == result) {
		game.h_figure = undefined;
		bars.onGameOver();
	}
};

function h_gameAppendBlocks(appended_list) {
	for(var i = 0; i < appended_list.length; i++) {
		pt = appended_list[i];
		depth_idx = game.h_de - pt.z - 1;
		elem = makeFillElement(game.h_size_multiplier, consts.getFillElemPlaneMaterial(depth_idx));
		elem.place(pt.x, pt.y, pt.z);
		elem.sceneAdd(game.h_scene);
		game.h_layers_blocks[pt.z].push(elem);
	}
};

function h_gameProcessTonnel() {
	var cx = game.h_tonnel.length;
	var cy = game.h_tonnel[0].length;
	var cz = game.h_tonnel[0][0].length;
	var upper_bound = 0;
	for(var z = cz-1; z >= upper_bound; z--) {
		done = true;
		for(var x = 0; x < cx; x++) {
			for(var y = 0; y < cy; y++) {
				if(! game.h_tonnel[x][y][z]) {
					done = false;
					x = cx;
					break;
				}
			}
		}
		if(! done) {
			continue;
		}
		// layer done. shifting layers down
		for(var x = 0; x < cx; x++) {
			for(var y = 0; y < cy; y++) {
				z_column_ref = game.h_tonnel[x][y];
				for(var iz = z; iz > upper_bound; iz--) {
					z_column_ref[iz] = z_column_ref[iz-1];
				}
				z_column_ref[upper_bound] = false;
			}
		}
		upper_bound++;
		z++; // back to check it again after shifting.
	}
	if(upper_bound != 0) {
		h_gameRefillTonnel();
		scorer.onLayersProcessed(upper_bound, game.getLayersInfo());
	}
	h_gameRemoveInvisibleBlocks();
};

function h_gameRefillTonnel() {
	// clear
	for(var i = 0; i < game.h_layers_blocks.length; i++) {
		h_gameRemoveLayersBlocks(i);
	}
	h_initBlocksData();
	// gather list of block's coordinates
	var cx = game.h_tonnel.length;
	var cy = game.h_tonnel[0].length;
	var cz = game.h_tonnel[0][0].length;
	list = [];
	for(var x = 0; x < cx; x++) {
		for(var y = 0; y < cy; y++) {
			z_column_ref = game.h_tonnel[x][y];
			for(var z = 0; z < cz; z++) {
				if(z_column_ref[z]) {
					list.push( {x: x, y: y, z: z} );
				}
			}
		}
	}
	// fill
	h_gameAppendBlocks(list);
};

function h_gameRemoveLayersBlocks(layer_index) {
	var layer_blocks = game.h_layers_blocks[layer_index];
	for(var i = 0; i < layer_blocks.length; i++) {
		layer_blocks[i].sceneRemove(game.h_scene);
	}
};

function h_gameRemoveInvisibleBlocks() {
	var cx = game.h_tonnel.length;
	var cy = game.h_tonnel[0].length;
	var cz = game.h_tonnel[0][0].length;
	function isDense(z) {
		for(var x = 0; x < cx; x++) {
			for(var y = 0; y < cy; y++) {
				var solid = game.h_tonnel[x][y][z] || game.h_tonnel[x][y][z-1];
				if(! solid) {
					return false;
				}
			}
		}
		return true;
	};
	var z = 0;
	// find top of the tonnel
	for( ; z < cz-2 ; z++) {
		if(game.h_layers_blocks[z]) {
			break;
		}
	}
	// check whether some two neighbour layers are dense
	for( ; z < cz-2; z++) {
		if(isDense(z)) {
			// cleaning all blocks starting from (z+2)'nd layer
			for(var iz = z+2; iz < cz; iz++) {
				h_gameRemoveLayersBlocks(iz);
				game.h_layers_blocks[iz] = [];
			}
			return;
		}
	}
};

function h_gamePause() {
	game.h_paused ^= true;
	game.h_prev_time = getTimeMillis() - game.h_prev_time; // time passed
};