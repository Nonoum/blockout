function generateFigureRepr(mesh_size, flags_array) {
	var repr = makeArray3d(mesh_size, mesh_size, mesh_size, false);
	var n = 0;
	for(var z = 0; z < mesh_size; z++) {
		for(var y = 0; y < mesh_size; y++) {
			for(var x = 0; x < mesh_size; x++) {
				if(n >= flags_array.length) {
					return repr;
				}
				repr[x][y][z] = flags_array[n];
				n++;
			}
		}
	}
	return repr;
};

function generateFigureGeometry(repr, graphic_size) {
	var geo = new THREE.Geometry();
	function checked(x, y, z) {
		if(x < 0) return false;
		if(x >= repr.length) return false;
		val = repr[x];
		if(y < 0) return false;
		if(y >= val.length) return false;
		val = val[y];
		if(z < 0) return false;
		if(z >= val.length) return false;
		return val[z];
	};
	function xDrawableLine(x, y, z) {
		return checked(x, y, z) ^ checked(x, y-1, z) ^ checked(x, y, z-1) ^ checked(x, y-1, z-1);
	};
	function yDrawableLine(x, y, z) {
		return checked(x, y, z) ^ checked(x-1, y, z) ^ checked(x, y, z-1) ^ checked(x-1, y, z-1);
	};
	function zDrawableLine(x, y, z) {
		return checked(x, y, z) ^ checked(x-1, y, z) ^ checked(x, y-1, z) ^ checked(x-1, y-1, z);
	};
	shift = -0.5 * repr.length * graphic_size;
	function addLine(x1, y1, z1, x2, y2, z2) {
		v = new THREE.Vector3(x1, y1, z1);
		v.multiplyScalar(graphic_size);
		v.addScalar(shift);
		geo.vertices.push(v);
		v = new THREE.Vector3(x2, y2, z2);
		v.multiplyScalar(graphic_size);
		v.addScalar(shift);
		geo.vertices.push(v);
	};
	mesh_size = repr.length + 1;
	for(var i = 0; i < mesh_size; i++) {
		for(var j = 0; j < mesh_size; j++) {
			for(var k = 0; k < mesh_size; k++) {
				if(xDrawableLine(i, j, k)) {
					addLine(i, j, k, i+1, j, k);
				}
				if(yDrawableLine(i, j, k)) {
					addLine(i, j, k, i, j+1, k);
				}
				if(zDrawableLine(i, j, k)) {
					addLine(i, j, k, i, j, k+1);
				}
			}
		}
	}
	return geo;
};

function generateFigureDrawable(figure_repr, graphic_size) {
	var lines = new THREE.Line( generateFigureGeometry(figure_repr, graphic_size), consts.figure_material );
	lines.type = THREE.LinePieces;
	return lines;
};