// object for calculating game score.

scorer = (function() {
	sc = [];
	sc.h_getMultiplier = function() {
		return (Math.floor(Math.pow(props.getLevel(), 1.2)) + 1) * geometries.getComplicationMultiplier(props.getSetId());
	};
	sc.h_addScore = function(score) {
		props.setScore(props.getScore() + score);
		achivs.onScoreAdded(props.getScore());
	};
	sc.h_getPlaneSize = function() {
		var met = props.getMetric();
		return met[0] * met[1];
	};
	//
	sc.onFigurePlaced = function(blocks_count) {
		props.setBlocksCount(props.getBlocksCount() + blocks_count);
		this.h_addScore(blocks_count * this.h_getMultiplier());
	};
	sc.onLayersProcessed = function(n_cleaned, layers_info) {
		if(! n_cleaned) {
			return;
		}
		var mult = this.h_getMultiplier() * this.h_getPlaneSize();
		if(layers_info.max_present == -1) { // fully cleaned
			mult *= 3;
		}
		this.h_addScore(mult * n_cleaned * n_cleaned);
		achivs.onLayersCleaned(n_cleaned, layers_info);
	};
	return sc;
})();