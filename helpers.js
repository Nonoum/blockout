/*
	Function for synchronizing game engine.

	returns current time in milliseconds from 1970's..
*/
function getTimeMillis() {
	return (new Date()).getTime();
};

/*
	Function for parsing incoming uri parameters.
	(uri) - uri string starting after '?'.

	returns dictionary with parsed parameters.
*/
function parseUriParams(uri) {
	var vs = uri.split('&');
	var list = [];
	for(var i = 0; i < vs.length; i++) {
		if(0 == vs[i].length) {
			continue;
		}
		var pair = vs[i].split('=');
		var name = decodeURIComponent(pair[0]);
		var val = decodeURIComponent(pair[1]);
		nn_assert(undefined == list[name], 'Parameter appeared second time: ' + uri);
		list[name] = val;
	}
	return list;
};

/*
	Function for forming URI string from properties.
	(properties) - the properties object with fields to pass in uri.
	(params) - dictionary with initial uri params, all to pass to uri.
	(keys_to_replace) - list of keys from (properties) to pass in uri.
	(to_uri_map) - dictionary 'properties_name':'uri_name' to replace param names in uri.

	returns formed uri string.
*/
function getPropertiesUri(properties, params, keys_to_replace, to_uri_map) {
	to_uri_map = to_uri_map || [];
	list = [];
	// cloning parameters list
	keys = Object.keys(params);
	for(var i = 0; i < keys.length; i++) {
		list[keys[i]] = params[keys[i]];
	}
	// replacing values from properties
	for(var i = 0; i < keys_to_replace.length; i++) {
		key = keys_to_replace[i];
		uri_key = (undefined == to_uri_map[key]) ? key : to_uri_map[key];
		list[uri_key] = properties[key];
	}
	// constructing uri
	keys = Object.keys(list);
	s = '';
	for(var i = 0; i < keys.length; i++) {
		s = s + keys[i] + '=' + list[keys[i]];
		if(i < keys.length-1) {
			s = s + '&';
		}
	}
	return s;
};

/*
	Allocates 3d array with [cx][cy][cz] sizes.
	(cx) - first size;
	(cy) - second size;
	(cz) - third size;
	(fill_value) - value to fill in cells.

	returns formed array.
*/
function makeArray3d(cx, cy, cz, fill_value) {
	var arr = [];
	for(var i = 0; i < cx; i++) {
		arr[i] = [];
		for(var j = 0; j < cy; j++) {
			arr[i][j] = [];
			for(var k = 0; k < cz; k++) {
				arr[i][j][k] = fill_value;
			}
		}
	}
	return arr;
};

/*
	Copies 3d array with [cx][cy][cz] sizes.
	(src) - source array;
	(dst) - destination array with at least same sizes;
	(cx) - first size;
	(cy) - second size;
	(cz) - third size;
*/
function copyArray3d(src, dst, cx, cy, cz) {
	for(var i = 0; i < cx; i++) {
		for(var j = 0; j < cy; j++) {
			for(var k = 0; k < cz; k++) {
				dst[i][j][k] = src[i][j][k];
			}
		}
	}
};

function objectToString(obj) {
	var keys = Object.keys(obj);
	var s = '';
	for(var i = 0; i < keys.length; i++) {
		s = s + keys[i] + ' : ' + obj[keys[i]] + '\n';
	}
	return s;
};