// figure descriptions as input parameters for generating figure representation (mesh size and flags array).

geometries = (function() {
	var g = [];
	// init figures
	g.h_figures = (function() {
		var fs = [];
		fs[0] = [3, [true,true,true,true]]; // L
		fs[1] = [2, [true,true,true]]; // angle
		fs[2] = [3, [true,true,true,false,true]]; // underpants
		fs[3] = [3, [true,true,false,false,true,true]]; // zig-zag
		fs[4] = [2, [true,true,
					true,false,
					true]]; // angle with voxel in center
		fs[5] = [2, [true,true,
					false,false,
					true,false,
					true]]; // angle with voxel at side
		// rest of basic flat figures
		fs[6] = [1, [true]]; // single voxel
		fs[7] = [2, [true, true]]; // two voxels
		fs[8] = [3, [true, true, true]]; // three voxels
		fs[9] = [2, [true, true,
					true, true]]; // square
		// extended figures
		fs[10] = [3, [true,true,true,
					false,true,false,
					false,false,false,
					false,false,false,
					false,true]]; // underpants +
		fs[11] = [3, [true,true,false,
					false,true,true,
					false,false,false,
					true]]; // zig-zag +
		fs[12] = [3, [true,true,true,
					true,false,false,
					false,false,false,
					false,false,false,
					true]]; // L +
		fs[13] = [2, [true,true,
					false,false,
					false,true,
					false,true]]; // angle with voxel at other side
		fs[14] = [3, [true,false,false,
					true,true,true,
					false,false,true]]; // wide zig-zag
		// larger figures
		fs[15] = [4, [true, true, true, true]]; // four voxels
		fs[16] = [4, [true, true, true, true, true]]; // long L
		return fs;
	})();
	// init figure sets
	g.h_sets = (function() {
		var s = [];
		s.push( {name: 'Classic', id: 0, size: 3, mult: 2, indexes: [0,1,2,3,4,5]} );
		s.push( {name: 'Flat', id: 1, size: 3, mult: 1, indexes: [0,1,2,3,6,7,8,9]} );
		s.push( {name: 'Extended', id: 2, size: 3, mult: 4, indexes: [0,1,2,3,4,5, 10,11,12,13,14]} );
		s.push( {name: 'Extended Large', id: 3, size: 4, mult: 5, indexes: [0,1,2,3,4,5, 10,11,12,13,14, 15,16]} );

		s.push( {name: 'All small', id: 4, size: 3, mult: 3, indexes: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14]} );
		s.push( {name: 'All', id: 5, size: 4, mult: 6, indexes: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]} );
		return s;
	})();

	// functions
	g.getSetsList = function(size) {
		var s = [];
		for(var i = 0; i < this.h_sets.length; i++) {
			if(this.h_sets[i].size <= size) {
				s.push( this.h_sets[i] );
			}
		}
		return s;
	};
	g.getRandomFigure = function(set_id) {
		idxs = this.h_sets[set_id].indexes;
		idxs_idx = Math.floor(Math.random() * (idxs.length-0.001));
		return this.h_figures[ idxs[idxs_idx] ];
	};
	g.getComplicationMultiplier = function(set_id) {
		return this.h_sets[set_id].mult;
	};
	return g;
})();