/*
	Creates single block element as composition of cube and it's corpse.
	(size_multiplier) - graphic size of single block;
	(plane_material) - Three.js material for block's verges.

	returns formed fill_element object.
*/
function makeFillElement(size_multiplier, plane_material) {
	var elem = [];
	elem.h_size = size_multiplier;
	{
		geo = generateFigureGeometry(generateFigureRepr(1, [true]), size_multiplier);
		var lines = new THREE.Line( geo, consts.fill_elem_line_material );
		lines.type = THREE.LinePieces;
		elem.corpse = lines;
	}
	{
		sz = size_multiplier * 0.96;
		geo = new THREE.BoxGeometry( sz, sz, sz );
		var cubic = new THREE.Mesh( geo, plane_material );
		elem.cubic = cubic;
	}
	elem.sceneAdd = h_addFillElement;
	elem.sceneRemove = h_removeFillElement;
	elem.place = h_placeFillElement;
	elem.place(0, 0, 0);
	return elem;
};

function h_addFillElement(scene) {
	scene.add( this.corpse );
	scene.add( this.cubic );
};

function h_removeFillElement(scene) {
	scene.remove( this.corpse );
	scene.remove( this.cubic );
};

function h_placeFillElement(x, y, z) {
	this.corpse.position.set(x, y, z);
	this.corpse.position.multiplyScalar(this.h_size);
	this.corpse.position.addScalar(this.h_size * 0.5);
	this.cubic.position.set(x, y, z);
	this.cubic.position.multiplyScalar(this.h_size);
	this.cubic.position.addScalar(this.h_size * 0.5);
};