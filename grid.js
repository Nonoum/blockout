function makeTonnelGrid(cx, cy, cz, block_size, sx, sy, sz) {
	var geometry = new THREE.Geometry();

	var end_x = sx + cx * block_size;
	var end_y = sy + cy * block_size;
	var end_z = sz + cz * block_size;
	function addLine(x1, y1, z1, x2, y2, z2) {
		geometry.vertices.push( new THREE.Vector3( x1, y1, z1 ) );
		geometry.vertices.push( new THREE.Vector3( x2, y2, z2 ) );
	};
	// z-rects
	for(var iz = 0; iz <= cz; iz++) {
		z = sz + iz * block_size;
		for(var i = 0; i < 2; i++) {
			y = (i == 0) ? sy : end_y;
			addLine(sx, y, z, end_x, y, z);
		}
		for(var i = 0; i < 2; i++) {
			x = (i == 0) ? sx : end_x;
			addLine(x, sy, z, x, end_y, z);
		}
	}
	// z-lines
	for(var ix = 0; ix <= cx; ix++) {
		x = sx + ix * block_size;
		for(var i = 0; i < 2; i++) {
			y = (i == 0) ? sy : end_y;
			addLine(x, y, sz, x, y, end_z);
		}
	}
	for(var iy = 1; iy < cy; iy++) {
		y = sy + iy * block_size;
		for(var i = 0; i < 2; i++) {
			x = (i == 0) ? sx : end_x;
			addLine(x, y, sz, x, y, end_z);
		}
	}
	// bottom mesh
	for(var ix = 1; ix < cx; ix++) {
		x = sx + ix * block_size;
		addLine(x, sy, end_z, x, end_y, end_z);
	}
	for(var iy = 1; iy < cy; iy++) {
		y = sy + iy * block_size;
		addLine(sx, y, end_z, end_x, y, end_z);
	}
	var lines = new THREE.Line( geometry, consts.grid_material );
	lines.type = THREE.LinePieces;
	return lines;
};