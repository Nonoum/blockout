// game bars controller.

bars = [];

function initBars(left_renderer, right_renderer, layers_count) {
	h_initLeftBar(left_renderer, layers_count);
	h_initRightBar(right_renderer);

	bars.onTimer = h_barsTimerMethod;
	bars.updateLayers = function(max_present) {
		bars.h_left.updateLayers(max_present);
	};
	bars.updateTimePlayed = function(seconds) {
		bars.h_right.updateTime(seconds);
	};
	bars.onGameOver = function() {
		bars.h_right.onGameOver();
	};
};

// common

function h_barsTimerMethod() {
	h_updateLeftBar();
	h_updateRightBar();
	this.h_left.render();
	this.h_right.render();
};

// left bar

function h_initLeftBar(renderer, layers_count) {
	var data = [];
	data.renderer = renderer;
	data.width = renderer.domElement.width;
	data.height = renderer.domElement.height;

	data.layers_count = layers_count;
	data.max_layer = -1;
	data.prev_max_layer = -1;
	data.level = -1;
	data.prev_level = -1;
	data.item_dist = (data.height - 48) / layers_count;
	data.item_height = data.item_dist - 2;
	// functions
	data.updateLayers = function(max_present_layer) {
		this.max_layer = max_present_layer;
	};
	data.updateLevel = function() {
		data.level = props.getLevel();
	};
	data.getCtx = function() {
		return this.renderer.domElement.getContext('2d');
	};
	data.initialDraw = function() {
		var ctx = this.getCtx();
		ctx.beginPath();
		ctx.rect(0, 0, this.width, this.height);
		ctx.fillStyle = consts.bars_bg_style;
		ctx.fill();

		ctx.strokeStyle = consts.bars_strokestyle;
		ctx.moveTo(this.width, 0);
		ctx.lineTo(this.width, this.height);

		ctx.moveTo(1, 40);
		ctx.lineTo(this.width-1, 40);
		ctx.stroke();
	};
	data.drawLayers = function(max_present_layer) {
		if(this.prev_max_layer == this.max_layer) {
			return;
		}
		function getItemStyle(depth) {
			var color = consts.getFillElemColor(depth);
			return 'rgb(' + (color >> 16) + ', ' + ((color & 0x00ff00) >> 8) + ', ' + (color & 0x0ff) + ')';
		};
		var ctx = this.getCtx();
		ctx.strokeStyle = consts.bars_bg_style;
		for(var i = 0; i < this.layers_count; i++) {
			var should_be = (i <= this.max_layer);
			var is_drawn = (i <= this.prev_max_layer);
			if(should_be ^ is_drawn) {
				ctx.beginPath();
				ctx.fillStyle = getItemStyle(i);

				ctx.rect(3, this.height - 3 - i*(this.item_dist) - this.item_height, this.width - 6, this.item_height);
				if(should_be && (! is_drawn)) {
					ctx.fillStyle = getItemStyle(i);
				} else {
					ctx.fillStyle = consts.bars_bg_style;
				}
				ctx.fill();
				ctx.stroke();
			}
		}
		this.prev_max_layer = this.max_layer;
	};
	data.drawLevel = function() {
		if(this.prev_level == this.level) {
			return;
		}
		var ctx = this.getCtx();
		// cleaning background
		ctx.beginPath();
		ctx.rect(2, 2, this.width-4, 37);
		ctx.fillStyle = consts.bars_bg_style;
		ctx.strokeStyle = consts.bars_bg_style;
		ctx.fill();
		ctx.stroke();
		// printing level text
		ctx.font="42px courier";
		ctx.fillStyle = consts.bars_dynamic_text_style;
		ctx.fillText('' + this.level, 12, 32);
		this.prev_level = this.level;
	};
	data.render = function() {
		this.drawLayers();
		this.drawLevel();
	};
	//
	data.initialDraw();
	bars.h_left = data; // left bar properties
};

function h_updateLeftBar() {
	bars.h_left.updateLevel();
};

// right bar

function h_initRightBar(renderer) {
	data = [];
	data.renderer = renderer;
	data.width = renderer.domElement.width;
	data.height = renderer.domElement.height;
	data.message = {show: false};
	// functions
	data.getCtx = function() {
		return this.renderer.domElement.getContext('2d');
	};
	data.initValues = function() {
		x = 5;
		h = 20;
		y = this.height - 8;
		var diff = 54;
		this.rects = [];
		this.rects['binds_info'] = {x: x, y: y - diff*4, w: this.width-x-2, h: h};
		this.rects['time'] = {x: x, y: y - diff*3, w: this.width-x-2, h: h};
		this.rects['score'] = {x: x, y: y - diff*2, w: this.width-x-2, h: h};
		this.rects['blocks'] = {x: x, y: y - diff, w: this.width-x-2, h: h};
		this.rects['pit'] = {x: x, y: y, w: this.width-x-2, h: h};
		this.values = [];
		this.values['time'] = 0;
		this.values['score'] = props.getScore();
		this.values['blocks'] = props.getBlocksCount();
	};
	data.initialDraw = function() {
		var ctx = this.getCtx();
		ctx.beginPath();
		ctx.rect(0, 0, this.width, this.height);
		ctx.fillStyle = consts.bars_bg_style;
		ctx.fill();
		ctx.strokeStyle = consts.bars_strokestyle;
		ctx.stroke();

		ctx.fillStyle = consts.bars_static_text_style;
		ctx.font="48px courier bold";
		ctx.fillText('B', 10, 40);
		ctx.font="28px courier bold";
		ctx.fillText('LOCK', 40, 28);
		ctx.font="36px courier bold";
		ctx.fillText('O', 53, 60);
		ctx.font="28px courier bold";
		ctx.fillText('UT', 80, 60);

		ctx.font="18px courier";
		var r = this.rects['binds_info'];
		ctx.fillText('ESC - exit', r.x, r.y - r.h);
		ctx.fillText('P - pause', r.x, r.y - r.h + r.h);
		var r = this.rects['time'];
		ctx.fillText('Played:', r.x, r.y - r.h - 2);
		var r = this.rects['score'];
		ctx.fillText('Score', r.x, r.y - r.h);
		var r = this.rects['blocks'];
		ctx.fillText('Blocks done', r.x, r.y - r.h);
		var r = this.rects['pit'];
		ctx.fillText('Pit', r.x, r.y - r.h);

		var metric = props.getMetric();
		var pit_text = '' + metric[0] + ' x ' + metric[1] + ' x ' + metric[2];
		ctx.fillStyle = consts.bars_dynamic_text_style;
		ctx.fillText(pit_text, r.x, r.y);
	};
	data.drawMessage = function() {
		var ctx = this.getCtx();
		if(this.message.show) {
			var time = getTimeMillis();
			var dt = (time - this.message.prev_time) / 1000.0;
			if(dt > 1.0) {
				this.message.prev_time = time;
				this.message.state ^= true;
				x = 6;
				y = 130;
				w = 116;
				h = 48;
				if(this.message.state) {
					ctx.font="24px serif";
					ctx.fillStyle = consts.bars_message_text_style;
					ctx.fillText('Game over', x, y-20);
					ctx.fillText('press ESC', x+12, y);
				} else {
					ctx.beginPath();
					ctx.rect(x, y - h + 8, w, h);
					ctx.fillStyle = consts.bars_bg_style;
					ctx.strokeStyle = consts.bars_bg_style;
					ctx.fill();
					ctx.stroke();
				}
			}
		}
	};
	data.drawValues = function() {
		var ctx = this.getCtx();
		var keys = ['score', 'blocks', 'time'];
		var time_text = '';
		{
			sec = this.values['time'] % 60;
			min = Math.floor(this.values['time'] / 60) % 60;
			hou = Math.floor(this.values['time'] / 3600) % 24;
			time_text += ((hou < 10) ? ('0' + hou) : '' + hou) + ':';
			time_text += ((min < 10) ? ('0' + min) : '' + min) + ':';
			time_text += ((sec < 10) ? ('0' + sec) : '' + sec);
		}
		var texts = [this.values['score'], this.values['blocks'], time_text];
		ctx.font="18px courier";
		for(var i = 0; i < keys.length; i++) {
			key = keys[i];
			if(this.values[key] == this.values['prev_' + key]) {
				continue;
			}
			var r = this.rects[key];
			// cleaning background
			ctx.beginPath();
			ctx.rect(r.x, r.y - r.h + 2, r.w, r.h);
			ctx.fillStyle = consts.bars_bg_style;
			ctx.fill();
			ctx.strokeStyle = consts.bars_bg_style;
			ctx.stroke();

			ctx.fillStyle = consts.bars_dynamic_text_style;
			ctx.fillText('' + texts[i], r.x, r.y);
			this.values['prev_' + key] = this.values[key];
		}
	};
	data.render = function() {
		this.drawMessage();
		this.drawValues();
	};
	data.onGameOver = function() {
		this.message = {show: true, prev_time: getTimeMillis(), state: false };
	};
	data.updateTime = function(seconds) {
		this.values['time'] = Math.floor(seconds);
	};
	data.initValues();
	data.initialDraw();
	bars.h_right = data; // right bar properties
};

function h_updateRightBar() {
	bars.h_right.values['score'] = props.getScore();
	bars.h_right.values['blocks'] = props.getBlocksCount();
};