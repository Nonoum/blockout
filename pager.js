// function for moving between pages

function openPage(addr, extra_params) {
	props.appendUriParams(extra_params);
	var uri = props.get();
	var url = addr + ((uri.length > 0) ? '?' : '') + uri;
	window.location = url;
};